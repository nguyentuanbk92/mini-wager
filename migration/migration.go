package migration

import (
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/wager/wagermodel"

	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&wagermodel.Wager{}, &ordermodel.Order{})
	db.Exec("ALTER TABLE `orders` ADD CONSTRAINT `fk_wagers_orders` FOREIGN KEY (`wager_id`) REFERENCES `wagers`(`id`)")
}
