package orderbiz

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/pkg/common"
)

type ListOrderStore interface {
	List(ctx context.Context, args *ordermodel.ListOrdersArgs) ([]*ordermodel.Order, error)
}

type listOrderBiz struct {
	store ListOrderStore
}

func NewListOrderBiz(store ListOrderStore) *listOrderBiz {
	return &listOrderBiz{store: store}
}

func (biz *listOrderBiz) List(ctx context.Context, args *ordermodel.ListOrdersArgs) ([]*ordermodel.Order, error) {
	result, err := biz.store.List(ctx, args)

	if err != nil {
		return nil, common.ErrCannotListEntity(ordermodel.EntityName, err)
	}

	return result, nil
}
