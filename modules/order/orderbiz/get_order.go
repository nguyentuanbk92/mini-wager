package orderbiz

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/pkg/common"
)

type GetOrderStore interface {
	GetByCondition(ctx context.Context, conditions map[string]interface{}) (*ordermodel.Order, error)
}

type getOrderBiz struct {
	store GetOrderStore
}

func NewGetOrderBiz(store GetOrderStore) *getOrderBiz {
	return &getOrderBiz{store: store}
}

func (biz *getOrderBiz) Get(ctx context.Context, id int) (*ordermodel.Order, error) {
	result, err := biz.store.GetByCondition(ctx, map[string]interface{}{"id": id})
	if err != nil {
		return nil, common.ErrCannotListEntity(ordermodel.EntityName, err)
	}

	return result, nil
}
