package orderbiz

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
	"miniwager/pkg/pubsub"
	"time"
)

type CreateOrderStore interface {
	Create(ctx context.Context, args *ordermodel.Order) (*ordermodel.Order, error)
}

type GetWagerStore interface {
	GetByCondition(ctx context.Context, conditions map[string]interface{}) (*wagermodel.Wager, error)
}

type createOrderBiz struct {
	psub       pubsub.PubSub
	orderStore CreateOrderStore
	wagerStore GetWagerStore
}

func NewCreateOrderBiz(ps pubsub.PubSub, oStore CreateOrderStore, wStore GetWagerStore) *createOrderBiz {
	return &createOrderBiz{psub: ps, orderStore: oStore, wagerStore: wStore}
}

var (
	ErrBuyingPriceNotValid = common.NewCustomError(nil,
		"buying_price must be lesser or equal to current_selling_price of the Wager",
		"ErrBuyingPriceNotValid",
	)
	ErrWagerCurrentSellingPriceNotValid = common.NewCustomError(nil,
		"Can not buy order. Wager current selling price does not valid",
		"ErrWagerCurrentSellingPriceNotValid",
	)
)

func (biz *createOrderBiz) Create(ctx context.Context, args *ordermodel.CreateOrderArgs) (*ordermodel.Order, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	wager, err := biz.wagerStore.GetByCondition(ctx, map[string]interface{}{"id": args.WagerID})
	if err != nil {
		return nil, err
	}

	buyingPrice := args.BuyingPrice
	if *wager.CurrentSellingPrice <= 0 {
		return nil, ErrWagerCurrentSellingPriceNotValid
	}
	if buyingPrice > *wager.CurrentSellingPrice {
		return nil, ErrBuyingPriceNotValid
	}
	now := time.Now()
	order := &ordermodel.Order{
		WagerID:     args.WagerID,
		BuyingPrice: args.BuyingPrice,
		BoughtAt:    &now,
	}
	order, err = biz.orderStore.Create(ctx, order)
	if err != nil {
		return nil, err
	}

	// public event
	biz.psub.Publish(ctx, common.TopicOrderCreatedEvent, pubsub.NewMessage(order))
	return order, nil
}
