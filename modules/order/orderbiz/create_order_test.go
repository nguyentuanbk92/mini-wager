package orderbiz

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/pubsub"
	"testing"

	"github.com/stretchr/testify/assert"
)

type wagerStoreMock struct{}
type orderStoreMock struct{}
type pubSubMock struct{}

func (pubSubMock) Publish(ctx context.Context, channel pubsub.Topic, message *pubsub.Message) error {
	return nil
}

func (pubSubMock) Subscribe(ctx context.Context, channel pubsub.Topic) (ch <-chan *pubsub.Message, close func()) {
	return nil, nil
}

func (s *orderStoreMock) Create(ctx context.Context, args *ordermodel.Order) (*ordermodel.Order, error) {
	return &ordermodel.Order{}, nil
}

func (s *wagerStoreMock) GetByCondition(ctx context.Context, conditions map[string]interface{}) (*wagermodel.Wager, error) {
	price := 100.0
	return &wagermodel.Wager{
		CurrentSellingPrice: &price,
	}, nil
}

func TestCreateOrder(t *testing.T) {
	wagerStore := &wagerStoreMock{}
	orderStore := &orderStoreMock{}
	pubSub := pubSubMock{}

	orderBiz := NewCreateOrderBiz(pubSub, orderStore, wagerStore)

	ctx := context.Background()

	testcases := []struct {
		name       string
		input      *ordermodel.CreateOrderArgs
		errMessage string
	}{
		{
			name:       "Err missing wager ID",
			input:      &ordermodel.CreateOrderArgs{},
			errMessage: ordermodel.ErrMissingWagerID.Message,
		},
		{
			name: "Err buying price must be positive",
			input: &ordermodel.CreateOrderArgs{
				WagerID:     1,
				BuyingPrice: -1,
			},
			errMessage: ordermodel.ErrBuyingPriceMustBePositive.Message,
		},
		{
			name: "Err buying price greater than wager current selling price",
			input: &ordermodel.CreateOrderArgs{
				WagerID:     1,
				BuyingPrice: 200,
			},
			errMessage: ErrBuyingPriceNotValid.Message,
		},
		{
			name: "Create order success",
			input: &ordermodel.CreateOrderArgs{
				WagerID:     1,
				BuyingPrice: 50.5,
			},
			errMessage: "",
		},
	}

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			_, err := orderBiz.Create(ctx, tt.input)
			if err != nil {
				assert.Equal(t, tt.errMessage, err.Error())
				return
			}
			assert.Nil(t, err)
		})
	}
}
