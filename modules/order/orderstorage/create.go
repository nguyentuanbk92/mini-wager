package orderstorage

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/pkg/common"
)

func (s *sqlStore) Create(ctx context.Context, order *ordermodel.Order) (*ordermodel.Order, error) {
	db := s.db
	if err := db.Create(order).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	return order, nil
}
