package orderstorage

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/pkg/common"

	"gorm.io/gorm"
)

func (s *sqlStore) GetByCondition(ctx context.Context, conditions map[string]interface{}) (*ordermodel.Order, error) {
	var result ordermodel.Order

	db := s.db

	if err := db.Where(conditions).
		First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrEntityNotFound(ordermodel.EntityName, err)
		}
		return nil, common.ErrDB(err)
	}

	return &result, nil
}
