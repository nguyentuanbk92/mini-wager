package orderstorage

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/pkg/common"
)

func (s *sqlStore) List(ctx context.Context, args *ordermodel.ListOrdersArgs) ([]*ordermodel.Order, error) {
	var result []*ordermodel.Order
	db := s.db
	db = db.Table(ordermodel.Order{}.TableName())
	paging := args.Paging
	db = db.Offset((paging.Page - 1) * paging.Limit).Limit(paging.Limit)

	if args.WagerID != 0 {
		db = db.Where("wager_id = ?", args.WagerID)
	}

	if err := db.Order("id DESC").Find(&result).Error; err != nil {
		return nil, common.ErrDB(err)
	}
	return result, nil
}
