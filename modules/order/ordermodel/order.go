package ordermodel

import (
	"miniwager/pkg/common"
	"time"
)

const EntityName = "order"

type Order struct {
	common.SQLModel
	WagerID     int        `json:"wager_id" gorm:"column:wager_id"`
	BuyingPrice float64    `json:"buying_price" gorm:"column:buying_price;"`
	BoughtAt    *time.Time `json:"bought_at" gorm:"column:bought_at"`
}

func (Order) TableName() string {
	return "orders"
}

func (o *Order) GetOrderID() int {
	return o.ID
}

func (o *Order) GetWagerID() int {
	return o.WagerID
}
