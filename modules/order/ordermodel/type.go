package ordermodel

import "miniwager/pkg/common"

type CreateOrderArgs struct {
	BuyingPrice float64 `json:"buying_price"`
	WagerID     int     `json:"wager_id"`
}

var (
	ErrMissingWagerID            = common.NewCustomError(nil, "Missing wager ID", "ErrMissingWagerID")
	ErrBuyingPriceMustBePositive = common.NewCustomError(nil, "Buying price should be positive", "ErrBuyingPriceNotValid")
)

func (args *CreateOrderArgs) Validate() error {
	if args.WagerID <= 0 {
		return ErrMissingWagerID
	}
	if args.BuyingPrice <= 0 {
		return ErrBuyingPriceMustBePositive
	}
	return nil
}

type ListOrdersArgs struct {
	Paging  common.Paging
	WagerID int
}
