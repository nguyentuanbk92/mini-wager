package ginorder

import (
	"miniwager/modules/order/orderbiz"
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/order/orderstorage"
	"miniwager/modules/wager/wagerstorage"
	"miniwager/pkg/common"
	"miniwager/pkg/component"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CreateOrder(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args ordermodel.CreateOrderArgs
		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		wagerID, err := strconv.Atoi(c.Param("wager_id"))
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		args.WagerID = wagerID

		orderStore := orderstorage.NewSQLStore(appCtx.GetMainDBConnection())
		wagerStore := wagerstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := orderbiz.NewCreateOrderBiz(appCtx.GetPubSub(), orderStore, wagerStore)
		result, err := biz.Create(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusCreated, result)
	}
}
