package wagerbiz

import (
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
	"testing"

	"github.com/stretchr/testify/assert"
)

var wager = &wagermodel.Wager{
	SQLModel: common.SQLModel{
		ID: 1,
	},
	SellingPrice: 120.5,
}

var orders = []*ordermodel.Order{
	&ordermodel.Order{
		BuyingPrice: 50,
	},
}

func TestCalcWagerStatistic(t *testing.T) {
	testcases := []struct {
		name   string
		wager  *wagermodel.Wager
		orders []*ordermodel.Order
		output *WagerStatistic
	}{
		{
			name:   "1 order purchased",
			wager:  wager,
			orders: orders,
			output: &WagerStatistic{
				ID:                  wager.ID,
				CurrentSellingPrice: 120.5 - 50,
				PercentageSold:      common.Float64RoundDown(50/120.5*100, 2),
				AmountSold:          1,
			},
		},
		{
			name:  "2 order purchased",
			wager: wager,
			orders: []*ordermodel.Order{
				&ordermodel.Order{
					BuyingPrice: 50,
				},
				&ordermodel.Order{
					BuyingPrice: 20.5,
				},
			},
			output: &WagerStatistic{
				ID:                  wager.ID,
				CurrentSellingPrice: 120.5 - 50 - 20.5,
				PercentageSold:      common.Float64RoundDown((50+20.5)/120.5*100, 2),
				AmountSold:          2,
			},
		},
	}

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			res := calcWagerStatistic(tt.wager, tt.orders)
			assert.Equal(t, tt.output, res)
		})
	}
}
