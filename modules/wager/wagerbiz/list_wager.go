package wagerbiz

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

type ListWagerStore interface {
	List(ctx context.Context, args *wagermodel.ListWagersArgs) ([]*wagermodel.Wager, error)
}

type listWagerBiz struct {
	store ListWagerStore
}

func NewListWagerBiz(store ListWagerStore) *listWagerBiz {
	return &listWagerBiz{store: store}
}

func (biz *listWagerBiz) List(ctx context.Context, args *wagermodel.ListWagersArgs) ([]*wagermodel.Wager, error) {
	result, err := biz.store.List(ctx, args)

	if err != nil {
		return nil, common.ErrCannotListEntity(wagermodel.EntityName, err)
	}

	return result, nil
}
