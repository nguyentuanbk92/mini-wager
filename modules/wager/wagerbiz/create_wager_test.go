package wagerbiz

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"testing"

	"github.com/stretchr/testify/assert"
)

type wagerStoreMock struct{}

func (s *wagerStoreMock) Create(ctx context.Context, args *wagermodel.Wager) (*wagermodel.Wager, error) {
	return &wagermodel.Wager{}, nil
}

func TestCreateWager(t *testing.T) {
	store := &wagerStoreMock{}
	wagerBiz := NewCreateWagerBiz(store)
	ctx := context.Background()

	testcases := []struct {
		name       string
		input      *wagermodel.CreateWagerArgs
		errMessage string
	}{
		{
			name:       "Missing total_wager_value",
			input:      &wagermodel.CreateWagerArgs{},
			errMessage: wagermodel.ErrTotalWagerValueNotValid.Message,
		},
		{
			name: "total_wager_value negative",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue: -1,
			},
			errMessage: wagermodel.ErrTotalWagerValueNotValid.Message,
		},
		{
			name: "Missing odds",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue: 1,
			},
			errMessage: wagermodel.ErrOddsNotValid.Message,
		},
		{
			name: "odds negative",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue: 1,
				Odds:            -1,
			},
			errMessage: wagermodel.ErrOddsNotValid.Message,
		},
		{
			name: "Missing selling_percentage",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue: 1,
				Odds:            1,
			},
			errMessage: wagermodel.ErrSellingPercentageNotValid.Message,
		},
		{
			name: "selling_percentage greater than 100",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue:   1,
				Odds:              1,
				SellingPercentage: 250,
			},
			errMessage: wagermodel.ErrSellingPercentageNotValid.Message,
		},
		{
			name: "selling_percentage negative",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue:   1,
				Odds:              1,
				SellingPercentage: -100,
			},
			errMessage: wagermodel.ErrSellingPercentageNotValid.Message,
		},
		{
			name: "Missing selling_price",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue:   10,
				Odds:              5,
				SellingPercentage: 35,
				SellingPrice:      2,
			},
			errMessage: wagermodel.ErrSellingPriceNotValid.Message,
		},
		{
			name: "selling_price not valid",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue:   10,
				Odds:              5,
				SellingPercentage: 35,
				SellingPrice:      2,
			},
			errMessage: wagermodel.ErrSellingPriceNotValid.Message,
		},
		{
			name: "Success",
			input: &wagermodel.CreateWagerArgs{
				TotalWagerValue:   10,
				Odds:              5,
				SellingPercentage: 35,
				SellingPrice:      4,
			},
			errMessage: "",
		},
	}

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			_, err := wagerBiz.Create(ctx, tt.input)
			if err != nil {
				assert.Equal(t, tt.errMessage, err.Error())
			}
		})
	}
}
