package wagerbiz

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

type GetWagerStore interface {
	GetByCondition(ctx context.Context, conditions map[string]interface{}) (*wagermodel.Wager, error)
}

type getWagerBiz struct {
	store GetWagerStore
}

func NewGetWagerBiz(store GetWagerStore) *getWagerBiz {
	return &getWagerBiz{store: store}
}

func (biz *getWagerBiz) Get(ctx context.Context, id int) (*wagermodel.Wager, error) {
	result, err := biz.store.GetByCondition(ctx, map[string]interface{}{"id": id})
	if err != nil {
		return nil, common.ErrCannotListEntity(wagermodel.EntityName, err)
	}

	return result, nil
}
