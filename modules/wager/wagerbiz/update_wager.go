package wagerbiz

import (
	"context"
	"miniwager/modules/order/ordermodel"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

type UpdateWagerStore interface {
	Update(ctx context.Context, id int, args *wagermodel.Wager) error
	GetByCondition(ctx context.Context, conditions map[string]interface{}) (*wagermodel.Wager, error)
}

type ListOrderBiz interface {
	List(ctx context.Context, args *ordermodel.ListOrdersArgs) ([]*ordermodel.Order, error)
}

type updateWagerBiz struct {
	wStore UpdateWagerStore
	oBiz   ListOrderBiz
}

func NewUpdateWagerBiz(wagerStore UpdateWagerStore, orderStore ListOrderBiz) *updateWagerBiz {
	return &updateWagerBiz{wStore: wagerStore, oBiz: orderStore}
}

func (biz *updateWagerBiz) UpdateWagerStatistic(ctx context.Context, wagerID int) error {
	wager, err := biz.wStore.GetByCondition(ctx, map[string]interface{}{"id": wagerID})
	if err != nil {
		return common.ErrCannotListEntity(wagermodel.EntityName, err)
	}

	orders, err := biz.oBiz.List(ctx, &ordermodel.ListOrdersArgs{
		Paging: common.Paging{
			Limit: 10000,
		},
		WagerID: wagerID,
	})
	if err != nil {
		return err
	}

	statistic := calcWagerStatistic(wager, orders)

	cmd := &wagermodel.Wager{
		SQLModel: common.SQLModel{
			ID: wagerID,
		},
		CurrentSellingPrice: &statistic.CurrentSellingPrice,
		PercentageSold:      &statistic.PercentageSold,
		AmountSold:          &statistic.AmountSold,
	}

	return biz.wStore.Update(ctx, wagerID, cmd)
}

type WagerStatistic struct {
	ID                  int
	CurrentSellingPrice float64
	PercentageSold      float64
	AmountSold          float64
}

func calcWagerStatistic(wager *wagermodel.Wager, orders []*ordermodel.Order) *WagerStatistic {
	totalBuyPrice := 0.0
	for _, o := range orders {
		totalBuyPrice += o.BuyingPrice
	}
	// round down to 2 digits
	remain := wager.SellingPrice - totalBuyPrice
	currentSellingPrice := common.Float64RoundDown(remain, 2)

	percentageSold := totalBuyPrice / wager.SellingPrice * 100
	percentageSold = common.Float64RoundDown(percentageSold, 2)

	amountSold := float64(len(orders))

	return &WagerStatistic{
		ID:                  wager.ID,
		CurrentSellingPrice: currentSellingPrice,
		PercentageSold:      percentageSold,
		AmountSold:          amountSold,
	}
}
