package wagerbiz

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
	"time"
)

type CreateWagerStore interface {
	Create(ctx context.Context, args *wagermodel.Wager) (*wagermodel.Wager, error)
}

type createWagerBiz struct {
	store CreateWagerStore
}

func NewCreateWagerBiz(store CreateWagerStore) *createWagerBiz {
	return &createWagerBiz{store: store}
}

func (biz *createWagerBiz) Create(ctx context.Context, args *wagermodel.CreateWagerArgs) (*wagermodel.Wager, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	// selling_price must be specified as a positive decimal value to two decimal places
	// we reject value have more than 2 decimal places
	sellingPrice := args.SellingPrice
	if !common.ValidateNumberNoMoreThanDecimal(sellingPrice, 2) {
		return nil, common.NewCustomError(
			nil,
			"Selling price must be specified as a positive decimal value to two decimal places",
			"ErrSellingPriceNoMoreThan2Decimal",
		)
	}

	sellingPriceCondition := float64(args.TotalWagerValue * args.SellingPercentage / 100)
	if sellingPrice <= sellingPriceCondition {
		return nil, wagermodel.ErrSellingPriceNotValid
	}

	now := time.Now()
	wager := &wagermodel.Wager{
		TotalWagerValue:   args.TotalWagerValue,
		Odds:              args.Odds,
		SellingPercentage: args.SellingPercentage,
		SellingPrice:      sellingPrice,
		// current_selling_price should be the selling_price until a Buy Wager action is taken against this wager record
		CurrentSellingPrice: &sellingPrice,
		PlacedAt:            &now,
	}
	return biz.store.Create(ctx, wager)
}
