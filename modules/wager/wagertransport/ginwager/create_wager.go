package ginwager

import (
	"miniwager/modules/wager/wagerbiz"
	"miniwager/modules/wager/wagermodel"
	"miniwager/modules/wager/wagerstorage"
	"miniwager/pkg/common"
	"miniwager/pkg/component"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateWager(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args wagermodel.CreateWagerArgs

		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		store := wagerstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := wagerbiz.NewCreateWagerBiz(store)
		result, err := biz.Create(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusCreated, result)
	}
}
