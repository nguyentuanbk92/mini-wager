package ginwager

import (
	"miniwager/modules/wager/wagerbiz"
	"miniwager/modules/wager/wagermodel"
	"miniwager/modules/wager/wagerstorage"
	"miniwager/pkg/common"
	"miniwager/pkg/component"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListWager(appCtx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var args wagermodel.ListWagersArgs

		if err := c.ShouldBind(&args); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		args.Paging.Fulfill()

		store := wagerstorage.NewSQLStore(appCtx.GetMainDBConnection())
		biz := wagerbiz.NewListWagerBiz(store)
		result, err := biz.List(c.Request.Context(), &args)
		if err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, result)
	}
}
