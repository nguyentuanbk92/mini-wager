package wagerstorage

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"

	"gorm.io/gorm"
)

func (s *sqlStore) GetByCondition(ctx context.Context, conditions map[string]interface{}) (*wagermodel.Wager, error) {
	var result wagermodel.Wager

	db := s.db

	if err := db.Where(conditions).
		First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &result, nil
}
