package wagerstorage

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

func (s *sqlStore) List(ctx context.Context, args *wagermodel.ListWagersArgs) ([]*wagermodel.Wager, error) {
	var result []*wagermodel.Wager
	db := s.db
	db = db.Table(wagermodel.Wager{}.TableName())
	paging := args.Paging
	db = db.Offset((paging.Page - 1) * paging.Limit).Limit(paging.Limit)

	if err := db.Order("id DESC").Find(&result).Error; err != nil {
		return nil, common.ErrDB(err)
	}
	return result, nil
}
