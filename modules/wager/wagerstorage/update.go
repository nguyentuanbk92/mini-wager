package wagerstorage

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *wagermodel.Wager) error {
	db := s.db

	if err := db.Where("id = ?", id).Updates(data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
