package wagerstorage

import (
	"context"
	"miniwager/modules/wager/wagermodel"
	"miniwager/pkg/common"
)

func (s *sqlStore) Create(ctx context.Context, wager *wagermodel.Wager) (*wagermodel.Wager, error) {
	db := s.db
	if err := db.Create(wager).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	return wager, nil
}
