package wagermodel

import (
	"miniwager/pkg/common"
	"time"
)

const EntityName = "wager"

type Wager struct {
	common.SQLModel
	TotalWagerValue     int        `json:"total_wager_value" gorm:"column:total_wager_value;"`
	Odds                int        `json:"odds" gorm:"column:odds;"`
	SellingPercentage   int        `json:"selling_percentage" gorm:"column:selling_percentage;"`
	SellingPrice        float64    `json:"selling_price" gorm:"column:selling_price;"`
	CurrentSellingPrice *float64   `json:"current_selling_price" gorm:"column:current_selling_price;"`
	PercentageSold      *float64   `json:"percentage_sold" gorm:"column:percentage_sold;"`
	AmountSold          *float64   `json:"amount_sold" gorm:"column:amount_sold;"`
	PlacedAt            *time.Time `json:"placed_at" gorm:"column:placed_at;"`
}

func (Wager) TableName() string {
	return "wagers"
}
