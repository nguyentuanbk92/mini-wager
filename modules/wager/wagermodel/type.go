package wagermodel

import "miniwager/pkg/common"

type CreateWagerArgs struct {
	TotalWagerValue   int     `json:"total_wager_value"`
	Odds              int     `json:"odds"`
	SellingPercentage int     `json:"selling_percentage"`
	SellingPrice      float64 `json:"selling_price"`
}

var (
	ErrTotalWagerValueNotValid   = common.NewCustomError(nil, "Total wager value must be a positive integer above 0", "ErrTotalWagerValueNotValid")
	ErrOddsNotValid              = common.NewCustomError(nil, "Odds must be a positive integer above 0", "ErrOddsNotValid")
	ErrSellingPercentageNotValid = common.NewCustomError(nil, "Selling percentage must be an integer between 1 and 100", "ErrSellingPercentageNotValid")
	ErrSellingPriceNotValid      = common.NewCustomError(nil, "selling_price must be greater than total_wager_value * (selling_percentage / 100)", "ErrSellingPriceNotValid")
)

func (args *CreateWagerArgs) Validate() error {
	if args.TotalWagerValue <= 0 {
		return ErrTotalWagerValueNotValid
	}
	if args.Odds <= 0 {
		return ErrOddsNotValid
	}
	if args.SellingPercentage < 1 || args.SellingPercentage > 100 {
		return ErrSellingPercentageNotValid
	}
	return nil
}

type ListWagersArgs struct {
	Paging common.Paging
}
