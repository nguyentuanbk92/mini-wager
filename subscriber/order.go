package subscriber

import (
	"context"
	"miniwager/modules/order/orderstorage"
	"miniwager/modules/wager/wagerbiz"
	"miniwager/modules/wager/wagerstorage"
	"miniwager/pkg/component"
	"miniwager/pkg/pubsub"
)

type HasOrderID interface {
	GetOrderID() int
	GetWagerID() int
}

func HandleOrderCreated(appCtx component.AppContext) consumerJob {
	handler := func(ctx context.Context, message *pubsub.Message) error {
		data := message.Data().(HasOrderID)

		wagerStore := wagerstorage.NewSQLStore(appCtx.GetMainDBConnection())
		orderStore := orderstorage.NewSQLStore(appCtx.GetMainDBConnection())
		updateWagerBiz := wagerbiz.NewUpdateWagerBiz(wagerStore, orderStore)

		return updateWagerBiz.UpdateWagerStatistic(ctx, data.GetWagerID())
	}
	return consumerJob{
		Title:   "Wager update when order created",
		Handler: handler,
	}
}
