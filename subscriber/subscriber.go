package subscriber

import (
	"context"
	"log"
	"miniwager/pkg/common"
	"miniwager/pkg/component"
	"miniwager/pkg/pubsub"
)

type consumerJob struct {
	Title   string
	Handler func(context.Context, *pubsub.Message) error
}

type consumerEngine struct {
	appCtx component.AppContext
}

func NewConsumerEngine(ctx component.AppContext) *consumerEngine {
	return &consumerEngine{appCtx: ctx}
}

func (engine *consumerEngine) Start() error {
	engine.runConsume(common.TopicOrderCreatedEvent, HandleOrderCreated(engine.appCtx))

	return nil
}

func (engine *consumerEngine) runConsume(topic pubsub.Topic, job consumerJob) error {
	_pubsub := engine.appCtx.GetPubSub()
	ctx := context.Background()
	ch, _ := _pubsub.Subscribe(ctx, topic)
	log.Println("Run consumer for: ", job.Title)

	go func() {
		for {
			message := <-ch
			err := job.Handler(ctx, message)
			if err != nil {
				log.Println("Consumer ", job.Title, " was error: ", err)
			}
		}
	}()
	return nil
}
