package middleware

import (
	"miniwager/pkg/common"
	"miniwager/pkg/component"

	"github.com/gin-gonic/gin"
)

func Recover(ctx component.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				c.Header("Content-Type", "application/json")
				if appErr, ok := err.(*common.AppError); ok {
					c.AbortWithStatusJSON(appErr.StatusCode, appErr.ShortResponse())
					return
				}
				appErr := common.ErrInternal(err.(error))
				c.AbortWithStatusJSON(appErr.StatusCode, appErr.ShortResponse())
				panic(err)
				return
			}
		}()
		c.Next()
	}
}
