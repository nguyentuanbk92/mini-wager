package component

import (
	"miniwager/pkg/pubsub"

	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	GetPubSub() pubsub.PubSub
}

type appCtx struct {
	db   *gorm.DB
	psub pubsub.PubSub
}

func NewAppContext(db *gorm.DB, ps pubsub.PubSub) *appCtx {
	return &appCtx{db: db, psub: ps}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB {
	return ctx.db
}

func (ctx *appCtx) GetPubSub() pubsub.PubSub {
	return ctx.psub
}
