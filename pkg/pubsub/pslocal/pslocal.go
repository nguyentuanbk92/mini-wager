package pslocal

import (
	"context"
	"log"
	"miniwager/pkg/common"
	"miniwager/pkg/pubsub"
	"sync"
)

var _ pubsub.PubSub = &localPubSub{}

// A pubsub run locally
type localPubSub struct {
	locker       *sync.RWMutex
	messageQueue chan *pubsub.Message
	mapChannel   map[pubsub.Topic][]chan *pubsub.Message
}

func NewLocalPubSub() *localPubSub {
	ps := &localPubSub{
		locker:       new(sync.RWMutex),
		messageQueue: make(chan *pubsub.Message, 10000),
		mapChannel:   make(map[pubsub.Topic][]chan *pubsub.Message),
	}
	ps.run()
	return ps
}

func (ps *localPubSub) Publish(ctx context.Context, topic pubsub.Topic, data *pubsub.Message) error {
	data.SetChannel(topic)
	go func() {
		defer common.AppRecover()
		ps.messageQueue <- data
		log.Println("New event published in topic: ", topic, " with data: ", data.Data())
	}()
	return nil
}

func (ps *localPubSub) Subscribe(ctx context.Context, topic pubsub.Topic) (ch <-chan *pubsub.Message, close func()) {
	c := make(chan *pubsub.Message)

	ps.locker.Lock()
	if val, ok := ps.mapChannel[topic]; ok {
		val = append(val, c)
		ps.mapChannel[topic] = val
	} else {
		ps.mapChannel[topic] = []chan *pubsub.Message{c}
	}
	ps.locker.Unlock()

	return c, func() {
		log.Println("Unsubcribe topic: ", topic)
		chans, ok := ps.mapChannel[topic]
		if !ok {
			return
		}
		for i := range chans {
			if chans[i] == c {
				// remove element at index i in chans
				chans = append(chans[:i], chans[i+1:]...)
				ps.locker.Lock()
				ps.mapChannel[topic] = chans
				ps.locker.Unlock()
				break
			}
		}
	}
}

func (ps *localPubSub) run() {
	log.Println("Run pubsub")
	go func() {
		for {
			msg := <-ps.messageQueue
			subs, ok := ps.mapChannel[msg.Channel()]
			if !ok {
				return
			}
			for i := range subs {
				go func(c chan *pubsub.Message) {
					c <- msg
				}(subs[i])
			}
		}
	}()
}
