package common

import "miniwager/pkg/pubsub"

const (
	TopicOrderCreatedEvent pubsub.Topic = "TopicOrderCreatedEvent"
)
