package common

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func TestValidateNumberNoMoreThanDecimal(t *testing.T) {
	testcases := []struct {
		name    string
		number  float64
		decimal int
		output  bool
	}{
		{
			name:    "2 decimal places",
			number:  10.25,
			decimal: 2,
			output:  true,
		},
		{
			name:    "Err 2 decimal places",
			number:  10.255,
			decimal: 2,
			output:  false,
		},
		{
			name:    "0 decimal places",
			number:  10,
			decimal: 2,
			output:  true,
		},
	}

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			res := ValidateNumberNoMoreThanDecimal(tt.number, tt.decimal)
			assert.Equal(t, tt.output, res)
		})
	}
}
