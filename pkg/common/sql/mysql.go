package sql

import (
	"fmt"
)

type ConfigMySQL struct {
	Host      string `yaml:"host"`
	Port      int    `yaml:"port"`
	Username  string `yaml:"username"`
	Password  string `yaml:"password"`
	Database  string `yaml:"database"`
	DebugMode bool   `yaml:"debug_mode"`
}

// ConnectionString ...
func (c *ConfigMySQL) ConnectionString() (connStr string) {
	connStr = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local", c.Username, c.Password, c.Host, c.Port, c.Database)
	return connStr
}
