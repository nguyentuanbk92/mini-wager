package sql

import (
	"fmt"
	"log"
)

type ConfigPostgres struct {
	Protocol string `yaml:"protocol"`
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
	SSLMode  string `yaml:"sslmode"`
	Timeout  int    `yaml:"timeout"`

	MaxOpenConns    int `yaml:"max_open_conns"`
	MaxIdleConns    int `yaml:"max_idle_conns"`
	MaxConnLifetime int `yaml:"max_conn_lifetime"`

	GoogleAuthFile string `yaml:"google_auth_file"`
}

// ConnectionString ...
func (c *ConfigPostgres) ConnectionString() (driver string, connStr string) {
	sslmode := c.SSLMode
	if c.SSLMode == "" {
		sslmode = "disable"
	}
	if c.Timeout == 0 {
		c.Timeout = 15
	}

	switch c.Protocol {
	case "":
		connStr = fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=%v connect_timeout=%v", c.Host, c.Port, c.Username, c.Password, c.Database, sslmode, c.Timeout)
	case "cloudsql":
		connStr = fmt.Sprintf("host=%v user=%v password=%v dbname=%v sslmode=disable",
			c.Host, c.Username, c.Password, c.Database)
	default:
		log.Fatalf("postgres: Invalid protocol. Config: %v", c)
	}
	return c.Driver(), connStr
}

func (c *ConfigPostgres) Driver() string {
	switch c.Protocol {
	case "", "postgres":
		return "postgres"
	case "cloudsql":
		return "cloudsqlpostgres"
	default:
		return "unknown"
	}
}
