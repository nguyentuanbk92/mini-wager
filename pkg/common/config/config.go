package cc

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"

	"miniwager/pkg/common/redis"
	"miniwager/pkg/common/sql"

	"gopkg.in/yaml.v2"
)

var (
	flConfigFile = ""
	flConfigYaml = ""
	flExample    = false
	flNoEnv      = false
	flCommitMsgs = false
)

func InitFlags() {
	flag.StringVar(&flConfigFile, "config-file", "", "Path to config file")
	flag.StringVar(&flConfigYaml, "config-yaml", "", "Config as yaml string")
	flag.BoolVar(&flNoEnv, "no-env", false, "Don't read config from environment")
	flag.BoolVar(&flExample, "example", false, "Print example config then exit")
}

func ParseFlags() {
	flag.Parse()
}

func FlagConfigFile() string {
	return flConfigFile
}

func FlagExample() bool {
	return flExample
}

func FlagNoEnv() bool {
	return flNoEnv
}

func LoadWithDefault(v, def interface{}) (err error) {
	defer func() {
		if flExample {
			if err != nil {
				fmt.Printf("Error while loading config: %v\n", err)
				panic(err.Error())
			}
			PrintExample(v)
			os.Exit(2)
		}
	}()

	if (flConfigFile != "") && (flConfigYaml != "") {
		return errors.New("must provide only -config-file or -config-yaml")
	}
	if flConfigFile != "" {
		err = LoadFromFile(flConfigFile, v)
		if err != nil {
			fmt.Printf("can not load config from file: %v (%v)", flConfigFile, err)
		}
		return err
	}
	if flConfigYaml != "" {
		return LoadFromYaml([]byte(flConfigYaml), v)
	}
	reflect.ValueOf(v).Elem().Set(reflect.ValueOf(def))
	return nil
}

// LoadFromFile loads config from file
func LoadFromFile(configPath string, v interface{}) (err error) {
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}
	return LoadFromYaml(data, v)
}

func LoadFromYaml(input []byte, v interface{}) (err error) {
	return yaml.Unmarshal(input, v)
}

func EnvPrefix(prefix []string, def string) string {
	if len(prefix) > 0 {
		return prefix[1]
	}
	return def
}

type EnvMap map[string]interface{}

func (m EnvMap) MustLoad() {
	if flNoEnv {
		return
	}

	for k, v := range m {
		MustLoadEnv(k, v)
	}
}

func MustLoadEnv(env string, val interface{}) {
	if flNoEnv {
		return
	}

	s := os.Getenv(env)
	if s == "" {
		return
	}

	v := reflect.ValueOf(val)
	if v.Kind() != reflect.Ptr {
		fmt.Printf("Expect pointer for env: %v", env)
		panic("Expect pointer for env")
	}

	v = v.Elem()
	switch v.Kind() {
	case reflect.String:
		v.SetString(s)
	case reflect.Bool:
		b, err := strconv.ParseBool(s)
		if err != nil {
			fmt.Printf("%v expects a boolean, got: %v", s)
		}
		v.SetBool(b)
	case reflect.Int, reflect.Int64:
		i, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			fmt.Printf("%v expects an integer, got: %v", s)
		}
		v.SetInt(i)
	case reflect.Uint, reflect.Uint64:
		i, err := strconv.ParseUint(s, 10, 64)
		if err != nil {
			fmt.Printf("%v expects an unsigned integer, got: %v", s)
		}
		v.SetUint(i)
	case reflect.Float64:
		i, err := strconv.ParseFloat(s, 64)
		if err != nil {
			fmt.Printf("%v expects an float64, got: %v", s)
		}
		v.SetFloat(i)
	default:
		fmt.Printf("Unexpected type for env: %v", s)
	}
}

// PrintExample prints example config
func PrintExample(cfg interface{}) {
	data, err := yaml.Marshal(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Println(string(data))
}

type Postgres = sql.ConfigPostgres

// DefaultPostgres ...
func DefaultPostgres() Postgres {
	return Postgres{
		Protocol:       "",
		Host:           "postgres",
		Port:           5432,
		Username:       "postgres",
		Password:       "postgres",
		Database:       "test",
		SSLMode:        "",
		Timeout:        15,
		GoogleAuthFile: "",
	}
}

func PostgresMustLoadEnv(c *sql.ConfigPostgres, prefix ...string) {
	p := "ET_POSTGRES"
	if len(prefix) > 0 {
		p = prefix[0]
	}
	EnvMap{
		p + "_PORT":     &c.Port,
		p + "_HOST":     &c.Host,
		p + "_SSLMODE":  &c.SSLMode,
		p + "_USERNAME": &c.Username,
		p + "_PASSWORD": &c.Password,
		p + "_DATABASE": &c.Database,
		p + "_TIMEOUT":  &c.Timeout,
	}.MustLoad()
}

type MySQL = sql.ConfigMySQL

// DefaultPostgres ...
func DefaultMySQL() MySQL {
	return MySQL{
		Host:     "127.0.0.1",
		Port:     3306,
		Username: "mysql",
		Password: "mysql",
		Database: "test",
	}
}

func MySQLMustLoadEnv(c *sql.ConfigMySQL, prefix ...string) {
	p := "ET_MYSQL"
	if len(prefix) > 0 {
		p = prefix[0]
	}
	EnvMap{
		p + "_PORT":     &c.Port,
		p + "_HOST":     &c.Host,
		p + "_USERNAME": &c.Username,
		p + "_PASSWORD": &c.Password,
		p + "_DATABASE": &c.Database,
	}.MustLoad()
}

// HTTP ...
type HTTP struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

func (c *HTTP) MustLoadEnv(prefix string) {
	p := prefix
	EnvMap{
		p + "_HORT": &c.Host,
		p + "_PORT": &c.Port,
	}.MustLoad()
}

// Address ...
func (c HTTP) Address() string {
	if c.Port == 0 {
		log.Panic("Missing HTTP port")
	}
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

type Redis = redis.Redis

// DefaultRedis ...
func DefaultRedis() Redis {
	return Redis{
		Host:     "redis",
		Port:     "6379",
		Username: "",
		Password: "",
	}
}

func RedisMustLoadEnv(c *Redis, prefix ...string) {
	p := "ET_REDIS"
	if len(prefix) > 0 {
		p = prefix[0]
	}
	EnvMap{
		p + "_PORT":     &c.Port,
		p + "_HOST":     &c.Host,
		p + "_USERNAME": &c.Username,
		p + "_PASSWORD": &c.Password,
	}.MustLoad()
}
