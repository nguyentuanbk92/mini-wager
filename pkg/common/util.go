package common

import "math"

func Float64RoundDown(num float64, decimal int) float64 {
	round := math.Pow(10, float64(decimal))
	return math.Floor(num*round) / round
}

func ValidateNumberNoMoreThanDecimal(num float64, decimal int) bool {
	round := math.Pow(10, float64(decimal))

	return num*round-float64(int(num*round)) == 0
}
