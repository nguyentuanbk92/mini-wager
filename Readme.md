# Mini Wager

## 1. Requirements
Implement three endpoints to list/place/buy wagers

## 2. Architecture
The following diagram is introduced in Uncle Bob’s original blog about Clean Architecture

![clean-architecture](./public/images/clean-architecture.png)

We apply some principles of this architecture:
- Your component can only depend on one inner layer from the outer layer (layer design pattern)
- Each component should be coupled through the interface if the coupling is across the layers. And dependency must be solved by Dependency Injection

So we using the simple architecture:
![simple-clean-architecture](./public/images/simple-layers-clean-architecture-1.png)
- **Transport**: Handle HTTP requests, return data to Client
- **Business**: Implement business logic
- **Repository** (optional): Prepare needed data for business logic
- **Storage**: Store and retrieve data

## 3. How to start

### How to get default config file
```bash
go build -o mini-wager ./cmd/wager

./mini-wager -example
```

### How to run with custom config file
```bash
./mini-wager -config-file <path/to/config-file>
```

### Using docker compose

Run docker compose with default config or you can change config if you want
```bash
docker compose up -d
```

## 4. Checklist
- [x] The application must be run in Docker, candidate must provide docker-composer.yml and start.sh bash script at the root of the project, which should setup all relevant services/applications.
- [x] POST `/wagers` - Endpoint to create a wager
  - [x] To create a wager you must supply total_wager_value, odds, selling_percentage, selling_price
  - [x] The API responds with an object of the wager 
- [x] POST `/buy/:wager_id` - Endpoint to accept buying a wager, or part of a wager (fractional)
  - [x] A purchase can be made multiple times against a single wager while current_selling_price is still positive.
  - [x] A successful purchase should update the wager fields current_selling_price, percentage_sold, amount_sold
  - [x] current_selling_price = selling_price - sum(all previous buying_price for that wager)
- [x] GET `/wagers?page=:page&limit=:limit` - Endpoint to list wagers
- [x] The request input should be validated before processing. The server should return proper error responses in case validation fails.
- [x] All responses must be in json format for success and failure responses.

