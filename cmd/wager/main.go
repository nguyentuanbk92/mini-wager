package main

import (
	"log"
	"miniwager/cmd/wager/config"
	"miniwager/migration"
	"miniwager/modules/order/ordertransport/ginorder"
	"miniwager/modules/wager/wagertransport/ginwager"
	"miniwager/pkg/common/cmenv"
	cc "miniwager/pkg/common/config"
	"miniwager/pkg/component"
	"miniwager/pkg/middleware"
	"miniwager/pkg/pubsub/pslocal"
	"miniwager/subscriber"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/k0kubun/pp/v3"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	cc.InitFlags()
	cc.ParseFlags()

	// load config
	cfg, err := config.Load()

	if err != nil {
		log.Fatal("can not load config")
	}
	cmenv.SetEnvironment("wager-server", cfg.Env)
	if cmenv.IsDev() {
		pp.Println("config ::", cfg)
	}

	// connect db
	dsn := cfg.MySQL.ConnectionString()
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln(err)
	}

	if cfg.MySQL.DebugMode {
		db = db.Debug()
	}
	// migration
	migration.Migrate(db)

	if err := runService(cfg, db); err != nil {
		log.Fatalln(err)
	}
}

func runService(cfg config.Config, db *gorm.DB) error {
	localPubSub := pslocal.NewLocalPubSub()
	appCtx := component.NewAppContext(db, localPubSub)

	if err := subscriber.NewConsumerEngine(appCtx).Start(); err != nil {
		log.Fatalln(err)
	}

	r := gin.Default()
	r.Use(middleware.Recover(appCtx))
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	wagers := r.Group("/wagers")
	{
		wagers.POST("", ginwager.CreateWager(appCtx))
		wagers.GET("", ginwager.ListWager(appCtx))
	}
	r.POST("/buy/:wager_id", ginorder.CreateOrder(appCtx))
	return r.Run(":" + strconv.Itoa(cfg.HTTP.Port))
}
