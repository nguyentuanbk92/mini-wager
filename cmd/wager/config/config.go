package config

import (
	"miniwager/pkg/common/cmenv"
	cc "miniwager/pkg/common/config"
)

type Config struct {
	HTTP  cc.HTTP  `yaml:"http"`
	MySQL cc.MySQL `yaml:"mysql"`
	Env   string   `yaml:"env"`
}

func Default() Config {
	cfg := Config{
		HTTP: cc.HTTP{Port: 8080},
		MySQL: cc.MySQL{
			Host:      "db",
			Port:      3306,
			Username:  "wager",
			Password:  "VpQj3=4n",
			Database:  "wager",
			DebugMode: true,
		},
		Env: cmenv.EnvDev.String(),
	}
	return cfg
}

// Load loads config from file
func Load() (Config, error) {
	var cfg, defCfg Config
	defCfg = Default()
	err := cc.LoadWithDefault(&cfg, defCfg)
	if err != nil {
		return cfg, err
	}
	cc.MySQLMustLoadEnv(&cfg.MySQL)
	return cfg, err
}
